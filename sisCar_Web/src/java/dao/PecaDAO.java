package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Pecas;

import util.ConnectionDAO;
import util.DAO;

public class PecaDAO implements DAO {

    private Connection conn;
    private int lastID;

    public PecaDAO() throws Exception {

    }

    @Override
    public void atualizar(Object ob) throws Exception {
        Pecas peca = (Pecas) ob;

        if (peca != null) {
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                conn = ConnectionDAO.getConnection();
                String SQL = "UPDATE siscar.pecas SET descricao=?, valor=?, quantidade=?, nome=? WHERE siscar.pecas.idpecas =?;";
                ps = conn.prepareStatement(SQL);
                ps.setString(1, peca.getDescricao());
                ps.setFloat(2, peca.getValor());
                ps.setInt(3, peca.getQuant());
                ps.setString(4, peca.getNome());
                ps.setInt(5, peca.getIdPecas());

                int affectedRows = ps.executeUpdate();

                if (affectedRows == 0) {
                    throw new Exception("N�o foi possivel excluir!");
                }

            } catch (SQLException sqle) {
                throw new Exception("Erro ao remover curso: \n" + sqle);
            } finally {
                ConnectionDAO.closeConnection(conn, ps);
            }

        } else {
            throw new Exception("Objeto Passado é nulo!");
        }

    }

    @Override
    public void excluir(Object ob) throws Exception {
        Pecas pecas = (Pecas) ob;

        if (pecas != null) {
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                conn = ConnectionDAO.getConnection();
                String SQL = "DELETE FROM siscar.pecasorcadas WHERE siscar.pecasorcadas.id_pecas = ?; "
                        + "DELETE FROM siscar.pecas WHERE siscar.pecas.idpecas = ?;";
                ps = conn.prepareStatement(SQL);
                ps.setInt(1, pecas.getIdPecas());
                ps.setInt(2, pecas.getIdPecas());

                int affectedRows = ps.executeUpdate();

                if (affectedRows == 0) 
                    throw new Exception("N�o foi possivel excluir!");
            } catch (SQLException sqle) {
                throw new Exception("Erro ao remover peca: \n" + sqle);
            } finally {
                ConnectionDAO.closeConnection(conn, ps);
            }

        } else {
            throw new Exception("Objeto Passado é nulo!");
        }
    }

    @Override
    public List listaTodos() throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Pecas> peca = new ArrayList<>();

        try {
            String SQL = "SELECT * FROM siscar.pecas";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);
            rs = ps.executeQuery();
            while (rs.next()) {
                peca.add(new Pecas(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getFloat(4), rs.getInt(5)));
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao Carregar dados: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
        return peca;
    }

    @Override
    public List procura(Object ob) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Pecas> pecas = new ArrayList<>();
        try {
            String SQL = "SELECT * FROM siscar.pecas WHERE siscar.pecas.descricao LIKE ? ";
            ps = ConnectionDAO.getConnection().prepareStatement(SQL);
            ps.setString(1, "%" + (String) ob + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                pecas.add(new Pecas(rs.getInt(1), rs.getString(5), rs.getString(2), rs.getFloat(3), rs.getInt(4)));
                //System.out.println(rs.getInt(1) +" "+ rs.getString(5)+" " +rs.getString(2)+" "+rs.getInt(3)+" "+rs.getInt(4));
            }

        } catch (SQLException sqle) {
            throw new Exception("Erro ao carregar pecas: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
        return pecas;
    }

    public int getLastID() {
        return this.lastID;
    }

    @Override
    public void salvar(Object ob) throws Exception {
        Pecas peca = (Pecas) ob;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = ConnectionDAO.getConnection();
            String SQL = "INSERT INTO siscar.pecas(descricao, valor, quantidade, nome) VALUES ( ?, ?, ?, ?);";
            //ps = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            ps = conn.prepareStatement(SQL, java.sql.Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, peca.getDescricao());
            ps.setFloat(2, peca.getValor());
            ps.setInt(3, peca.getQuant());
            ps.setString(4, peca.getNome());
            //ps.setInt(3, curso.getCoordenador());
            //tratando da inserção e coletando último id;

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException();
            }

            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                this.lastID = rs.getInt(1);
            } else {
                this.lastID = 0;
            }

        } catch (SQLException sqle) {
            if (conn != null) {
                try {
                    System.err.print("Falha transacao em Rollback!!!!");
                    conn.rollback();
                } catch (SQLException sqlE) {
                    sqlE.printStackTrace();
                }
            }
            throw new Exception("Erro ao salvar dados do curso: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }
}
