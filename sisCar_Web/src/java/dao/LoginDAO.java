/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.LoginModel;
import util.ConnectionDAO;
import util.DAO;

/**
 *
 * @author Usuário
 */
public class LoginDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void excluir(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listaTodos() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List procura(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void salvar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   public List procuraLogin(LoginModel ob) throws Exception {
                Connection conn = null;  
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<LoginModel> login = new ArrayList<>();
		try{
			String SQL = "select siscar.usuario.email,siscar.usuario.senha,siscar.usuario.tipousuario_idtipousuario from siscar.usuario where email = ? and senha = ?;";
			ps = ConnectionDAO.getConnection().prepareStatement(SQL);
			ps.setString(1, ob.getEmail());
                        ps.setString(2, ob.getSenha());
			rs = ps.executeQuery();
			while(rs.next()){
                                login.add(new LoginModel(rs.getString(1),rs.getString(2),rs.getInt(3)));
				//pecas.add(new Pecas(rs.getInt(1),rs.getString(5),rs.getString(2),rs.getFloat(3),rs.getInt(4)));
				//System.out.println(rs.getInt(1) +" "+ rs.getString(5)+" " +rs.getString(2)+" "+rs.getInt(3)+" "+rs.getInt(4));
			}

		}catch (SQLException sqle){
			throw new Exception("Erro ao carregar pecas: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
		return login;
	}
}
