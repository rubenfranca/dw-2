package model;

import classes.Pecas;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class PecaTableModel extends AbstractTableModel {
	
	private ArrayList<Pecas> dados;
	private String colunas[] = {"ID Peca", "Nome Peca", "Descricao", "Valor","Quantidade"};
	
	public PecaTableModel(){
		this.dados = new ArrayList<Pecas>();
	}
	public void addRow(Pecas p){
		this.dados.add(p);
		this.fireTableDataChanged();
	}
	public void removeRow(int linha) {
		this.dados.remove(linha);
		this.fireTableRowsDeleted(linha, linha);
	}
	public String getColumnName(int col) {
		return this.colunas[col];
	}
	@Override
	public int getColumnCount() {
		return this.colunas.length;
	}
	@Override
	public int getRowCount() {
		return this.dados.size();
	}

	@Override
	public Object getValueAt(int linha, int coluna) {
		switch(coluna) {
		case 0: return this.dados.get(linha).getIdPecas();
		case 1: return this.dados.get(linha).getNome();
		case 2: return this.dados.get(linha).getDescricao();
		case 3: return this.dados.get(linha).getValor();
		case 4: return this.dados.get(linha).getQuant();
		
		}
		return null;
	}
	public Pecas getRowAt(int linha) {
		return this.dados.get(linha);
	}	
    public ArrayList<Pecas> getCurso() {
		
		return this.dados;
	}

}
