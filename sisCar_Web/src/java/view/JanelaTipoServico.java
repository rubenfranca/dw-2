package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;

import dao.TipoServicoDAO;
import model.TipoServicoTableModel;
import classes.TipoServico;

public class JanelaTipoServico extends JFrame{
	
	private JTable tabelaTipoServico;
	private TipoServicoDAO tipoBanco;
	
	public JanelaTipoServico(){
		super("Janela de teste de indice");
		setSize(500,500);
		setLocationRelativeTo(null);		
		JPanel painel = new JPanel(new BorderLayout());
		JPanel painelSuperior = new JPanel(new GridLayout(0,2,0,0));
		//botao
		JButton bComIndice = new JButton("Com indice");
		JButton bSemIndice = new JButton("Sem Indice");
		
		//tabela
		tabelaTipoServico = new JTable();
		JScrollPane barraRolagem = new JScrollPane(tabelaTipoServico);
		TipoServicoTableModel modelo = new TipoServicoTableModel();
		tabelaTipoServico.setModel(modelo);
		painel.add("Center",barraRolagem);		
		painelSuperior.add(bComIndice);
		painelSuperior.add(bSemIndice);
		
		//acoes 
		bComIndice.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ListaTodosComIndice();
				
			}
		});
		bSemIndice.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ListaTodosSemIndice();
				
			}
		});
		
		painel.add("North",painelSuperior);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setContentPane(painel);
			
	}
	public void ListaTodosComIndice(){
		try {
			List<TipoServico> lista;
			tipoBanco = new TipoServicoDAO();
			lista = tipoBanco.listaTodosComIndice();
			TipoServicoTableModel modelo = new TipoServicoTableModel();
			for (TipoServico s: lista){
				modelo.addRow(s);
			}
			tabelaTipoServico.setModel(modelo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void ListaTodosSemIndice(){
		
		try {
			List<TipoServico> lista;
			tipoBanco = new TipoServicoDAO();
			lista = tipoBanco.listaTodos();
			TipoServicoTableModel modelo = new TipoServicoTableModel();
			for (TipoServico s: lista){
				modelo.addRow(s);
			}
			tabelaTipoServico.setModel(modelo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		JanelaTipoServico janela = new JanelaTipoServico();
		janela.setVisible(true);
	}

}
