<<<<<<< HEAD
CREATE SCHEMA siscar;

CREATE domain siscar.tipo as integer
DEFAULT 1 not null
=======
﻿CREATE SCHEMA siscar

CREATE domain siscar.tipo as integer
DEFAULT 1
not null
>>>>>>> origin/master
check(value in (1,2,3));

CREATE TABLE siscar.tipoUsuario (
  idtipoUsuario SERIAL,
  nome VARCHAR(50) NOT NULL,
  descricao VARCHAR(200) NULL,
  CONSTRAINT pk_idtipoUsuario PRIMARY KEY(idtipoUsuario)
);

INSERT INTO siscar.tipoUsuario(nome,descricao) VALUES('Mecanico', 'Funcionario que trabalha com os carros');
INSERT INTO siscar.tipoUsuario(nome,descricao) VALUES('Cliente', 'Cliente que terá acesso as funcionalidades do sistema');
INSERT INTO siscar.tipoUsuario(nome,descricao) VALUES('Administrador', 'Usuario com maior nivel de acesso no sistema');

CREATE TABLE siscar.Status(
  idStatus SERIAL,
  --Agenda_idAgenda INTEGER NOT NULL,
  descricao VARCHAR(255) not NULL,
  CONSTRAINT pk_id_status PRIMARY KEY(idStatus)
  --CONSTRAINT fk_agenda_idAgenda FOREIGN KEY(Agenda_idAgenda)
  --REFERENCES siscar.agenda (idAgenda)
);

INSERT INTO siscar.Status(descricao) VALUES ('Em Andamento');
INSERT INTO siscar.Status(descricao) VALUES ('Agendado');
INSERT INTO siscar.Status(descricao) VALUES ('Atrasado');
INSERT INTO siscar.Status(descricao) VALUES ('Pronto');

CREATE TABLE siscar.Usuario (
  idUsuario SERIAL ,
  tipoUsuario_idtipoUsuario siscar.tipo ,
  nome VARCHAR(255) NULL,
  sobrenome VARCHAR(255) NULL,
  setor VARCHAR(255) NULL,
  rua VARCHAR(255) NULL,
  email VARCHAR(255) NULL,
  CONSTRAINT pk_idUsuario PRIMARY KEY(idUsuario),
  CONSTRAINT fk_tipoUsuario FOREIGN KEY (tipoUsuario_idtipoUsuario)
  REFERENCES siscar.tipoUsuario (idtipoUsuario)
);

INSERT INTO siscar.Usuario(nome,sobrenome,setor,rua,email) VALUES ('Gui','Augusto','Rua Tal','Setor Tal','hotmail');
INSERT INTO siscar.Usuario(tipoUsuario_idtipoUsuario,nome,sobrenome,setor,rua,email) VALUES (1,'Guilherme','Augusto','Rua Tal','Setor Grande','hotmail');
INSERT INTO siscar.Usuario(tipoUsuario_idtipoUsuario,nome,sobrenome,setor,rua,email) VALUES (2,'Brunao Lamigod','Augusto','Rua Tal','Setor Caxias','hotmail');
INSERT INTO siscar.Usuario(tipoUsuario_idtipoUsuario,nome,sobrenome,setor,rua,email) VALUES (2,'Ruben','França','Rua da Cordona','Setor Freire','frança@hotmail.com');
INSERT INTO siscar.Usuario(tipoUsuario_idtipoUsuario,nome,sobrenome,setor,rua,email) VALUES (2,'Lucas','Urquiza','Rua Dias','Setor Albert','urquiza@hotmail.com');
INSERT INTO siscar.Usuario(tipoUsuario_idtipoUsuario,nome,sobrenome,setor,rua,email) VALUES (2,'Kaique','Silva','Rua Tartaruga','Setor Sao Lucas','silva@hotmail.com');
INSERT INTO siscar.Usuario(tipoUsuario_idtipoUsuario,nome,sobrenome,setor,rua,email) VALUES (2,'Alexandre','Bellezi','Rua Caixeta','Setor Tal','bellezi@hotmail.com');
INSERT INTO siscar.Usuario(tipoUsuario_idtipoUsuario,nome,sobrenome,setor,rua,email) VALUES (2,'Welison','Silva','Rua Tatuape','Setor Tal','welison@hotmail.com');

CREATE TABLE siscar.Agenda (
  idAgenda SERIAL,
  descricao varchar(200),
  CONSTRAINT pk_agenda PRIMARY KEY(idAgenda)
);

INSERT INTO siscar.Agenda(descricao) VALUES ('Agenda para Agendamento de Serviços');

CREATE TABLE siscar.Agendamento (
  Agenda_idAgenda INTEGER NOT NULL,
  Usuario_idUsuario INTEGER  NOT NULL,
  Status_idStatus INTEGER NOT NULL,
  data2 DATE NOT NULL,
  hora TIME NOT NULL,
  CONSTRAINT fk_usuario FOREIGN KEY (Usuario_idUsuario) REFERENCES siscar.usuario (idUsuario),
  CONSTRAINT fk_agenda FOREIGN KEY (Agenda_idAgenda) REFERENCES siscar.Agenda (idAgenda),
  CONSTRAINT fk_status FOREIGN KEY (Status_idStatus) REFERENCES siscar.Status (idStatus)
 );

INSERT INTO siscar.Agendamento(Agenda_idAgenda,Usuario_idUsuario,Status_idStatus,data2,hora) VALUES (1,1,2,'08/11/2017','08:20');
INSERT INTO siscar.Agendamento(Agenda_idAgenda,Usuario_idUsuario,Status_idStatus,data2,hora) VALUES (1,2,2,'08/11/2017','10:20');
INSERT INTO siscar.Agendamento(Agenda_idAgenda,Usuario_idUsuario,Status_idStatus,data2,hora) VALUES (1,3,2,'08/11/2017','11:20');
INSERT INTO siscar.Agendamento(Agenda_idAgenda,Usuario_idUsuario,Status_idStatus,data2,hora) VALUES (1,4,2,'09/11/2017','15:40');
INSERT INTO siscar.Agendamento(Agenda_idAgenda,Usuario_idUsuario,Status_idStatus,data2,hora) VALUES (1,5,2,'09/11/2017','09:20');

CREATE TABLE siscar.Pecas (
  idPecas SERIAL,
  nome VARCHAR(50) NOT NULL,
  descricao VARCHAR(255) NULL,
  valor numeric NOT NULL,
  quantidade INTEGER NOT NULL,
  CONSTRAINT pk_idPecas PRIMARY KEY(idPecas)
);

INSERT INTO siscar.Pecas(nome,descricao,valor,quantidade) VALUES('Alternador','Componente Carro',100.00,10);
INSERT INTO siscar.Pecas(nome,descricao,valor,quantidade) VALUES('Amortecedor','Componente da Suspensão do Carro',200.00,20);
INSERT INTO siscar.Pecas(nome,descricao,valor,quantidade) VALUES('Embreagem', 'Componente Essencial para Carro', 250.00, 15);
INSERT INTO siscar.Pecas(nome,descricao,valor,quantidade) VALUES('Cambio', 'Cambio para Carros Ford', 300.00, 5);
INSERT INTO siscar.Pecas(nome,descricao,valor,quantidade) VALUES('Engate', 'Engate Pequeno para Carretinha', 150.00, 10);

CREATE TABLE siscar.tipoServico (
  idtipoServico SERIAL,
  nome VARCHAR(50) NOT NULL,
  descricao VARCHAR(255) NULL,
  CONSTRAINT pk_idtipoServico PRIMARY KEY(idtipoServico)
);

INSERT INTO siscar.tipoServico(nome,descricao) VALUES('Hidraulico', 'Serviço Referente a Parte Hidraulica');
INSERT INTO siscar.tipoServico(nome,descricao) VALUES('Eletrico', 'Serviço Referente a Parte Eletrica do Carro');
INSERT INTO siscar.tipoServico(nome,descricao) VALUES('Pintura', 'Serviço para Pintura de Carro');
INSERT INTO siscar.tipoServico(nome,descricao) VALUES('Suspensão', 'Serviço para Suspensão do Carro');

CREATE TABLE siscar.servicos
(
  idservicos serial NOT NULL,
  nome character varying(60) NOT NULL,
  tiposervico_idtiposervico integer NOT NULL,
  temposervico integer NOT NULL,
  descricao character varying(255),
  valor integer NOT NULL,
  CONSTRAINT pk_idservicos PRIMARY KEY (idservicos),
  CONSTRAINT fk_tiposervico FOREIGN KEY (tiposervico_idtiposervico)
      REFERENCES siscar.tiposervico (idtiposervico)
 );
 
INSERT INTO siscar.Servicos(nome,tipoServico_idtipoServico,tempoServico,descricao,valor) VALUES('Troca de Amortecedor',1,2,'Troca de Amortecedor do Carro marca Ford',500);
INSERT INTO siscar.Servicos(nome,tipoServico_idtipoServico,tempoServico,descricao,valor) VALUES('Troca de Pneu',1,1, 'Troca de 4 pneu do carro', 300);
INSERT INTO siscar.Servicos(nome,tipoServico_idtipoServico,tempoServico,descricao,valor) VALUES('Troca de Suspensao',4,2, 'Troca de Suspensão do Carro', 600);
INSERT INTO siscar.Servicos(nome,tipoServico_idtipoServico,tempoServico,descricao,valor) VALUES('Troca de Bateria',2,1,'Trocar Bateria do Carro', 400);
INSERT INTO siscar.Servicos(nome,tipoServico_idtipoServico,tempoServico,descricao,valor) VALUES('Colocar Engate no Carro',1,1,'Inserção de Engate no Carro',200);

CREATE TABLE siscar.Telefone (
  idTelefone SERIAL,
  Usuario_idUsuario INTEGER  NOT NULL,
  ddd INTEGER NOT NULL,
  numero VARCHAR(45) NULL,
  codigoPais INTEGER NOT NULL,
  PRIMARY KEY(idTelefone),
  CONSTRAINT fk_usuario FOREIGN KEY (Usuario_idUsuario)
  REFERENCES siscar.usuario (idUsuario)
);

INSERT INTO siscar.Telefone(Usuario_idUsuario, ddd, numero, codigoPais) VALUES (1,62,'40028922',55);
INSERT INTO siscar.Telefone(Usuario_idUsuario, ddd, numero, codigoPais) VALUES (2,62,'98657899',55);
INSERT INTO siscar.Telefone(Usuario_idUsuario, ddd, numero, codigoPais) VALUES (1,62,'34567245',55);
INSERT INTO siscar.Telefone(Usuario_idUsuario, ddd, numero, codigoPais) VALUES (2,62,'33245678',55);

CREATE TABLE siscar.Orcamento (
  idOrcamento SERIAL,
  Agenda_idAgenda INTEGER NOT NULL,
  Usuario_idUsuario INTEGER NOT NULL,
  CONSTRAINT pk_idOrcamento PRIMARY KEY(idOrcamento),
  CONSTRAINT fk_usuario FOREIGN KEY (Usuario_idUsuario)
  REFERENCES siscar.usuario (idUsuario),
  CONSTRAINT fk_agenda FOREIGN KEY (Agenda_idAgenda)
  REFERENCES siscar.Agenda (idAgenda)
);

INSERT INTO siscar.Orcamento(Agenda_idAgenda,Usuario_idUsuario) VALUES (1,1);
INSERT INTO siscar.Orcamento(Agenda_idAgenda,Usuario_idUsuario) VALUES (1,2);
INSERT INTO siscar.Orcamento(Agenda_idAgenda,Usuario_idUsuario) VALUES (1,1);
INSERT INTO siscar.Orcamento(Agenda_idAgenda,Usuario_idUsuario) VALUES (1,3);
INSERT INTO siscar.Orcamento(Agenda_idAgenda,Usuario_idUsuario) VALUES (1,4);

CREATE TABLE siscar.pecasOrcadas(
  id SERIAL,
  id_pecas integer not null,
  id_orcamento integer not null,
  CONSTRAINT pk_idpecasOrcadas PRIMARY KEY (id),
  CONSTRAINT fk_idpecas FOREIGN KEY (id_pecas)
  REFERENCES siscar.pecas (idPecas),
  CONSTRAINT fk_idOrcamento FOREIGN KEY (id_orcamento)
  REFERENCES siscar.orcamento (idOrcamento)
);

INSERT INTO siscar.pecasOrcadas(id_pecas,id_orcamento) VALUES(1,1);
INSERT INTO siscar.pecasOrcadas(id_pecas,id_orcamento) VALUES(2,1);
INSERT INTO siscar.pecasOrcadas(id_pecas,id_orcamento) VALUES(3,2);
INSERT INTO siscar.pecasOrcadas(id_pecas,id_orcamento) VALUES(1,3);
INSERT INTO siscar.pecasOrcadas(id_pecas,id_orcamento) VALUES(5,4);

CREATE TABLE siscar.servicosOrcados(
  id SERIAL,
  id_servicos integer not null,
  id_orcamento integer not null,
  CONSTRAINT pk_idservicosOrcados PRIMARY KEY (id),
  CONSTRAINT fk_idServicos FOREIGN KEY (id_servicos)
  REFERENCES siscar.servicos (idServicos),
  CONSTRAINT fk_idOrcamento FOREIGN KEY (id_orcamento)
  REFERENCES siscar.orcamento (idOrcamento)
);

INSERT INTO siscar.servicosOrcados(id_servicos,id_orcamento) VALUES(1,1);
INSERT INTO siscar.servicosOrcados(id_servicos,id_orcamento) VALUES(1,2);
INSERT INTO siscar.servicosOrcados(id_servicos,id_orcamento) VALUES(2,2);
INSERT INTO siscar.servicosOrcados(id_servicos,id_orcamento) VALUES(3,3);
INSERT INTO siscar.servicosOrcados(id_servicos,id_orcamento) VALUES(5,4);

select * from siscar.usuario inner join siscar.orcamento on siscar.usuario.idusuario = siscar.orcamento.usuario_idusuario;

select * from siscar.servicos inner join siscar.tiposervico on siscar.servicos.tiposervico_idtiposervico = siscar.tiposervico.idtiposervico;

select * from siscar.tipoServico count(idtipoServico);

select * from siscar.tiposervico;

select * from siscar.agenda join siscar.orcamento on agenda.idagenda = orcamento.agenda_idagenda
