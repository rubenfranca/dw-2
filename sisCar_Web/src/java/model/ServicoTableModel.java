package model;

import classes.Servicos;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class ServicoTableModel extends AbstractTableModel {
	
	private ArrayList<Servicos> dados;
	private String colunas[] = {"ID Servico", "Nome","Tipo Servico", "Tempo", "Descricao", "Valor" };
	
	public ServicoTableModel(){
		this.dados = new ArrayList<Servicos>();
	}
	public void addRow(Servicos s){
		this.dados.add(s);
		this.fireTableDataChanged();
	}
	public void removeRow(int linha) {
		this.dados.remove(linha);
		this.fireTableRowsDeleted(linha, linha);
	}
	public String getColumnName(int col) {
		return this.colunas[col];
	}
	@Override
	public int getColumnCount() {
		return this.colunas.length;
	}
	@Override
	public int getRowCount() {
		return this.dados.size();
	}

	@Override
	public Object getValueAt(int linha, int coluna) {
		switch(coluna) {
		case 0: return this.dados.get(linha).getIdServicos();
		case 1: return this.dados.get(linha).getNome();
		case 2: return this.dados.get(linha).getTipoServico();
		case 3: return this.dados.get(linha).getTempoServico();
		case 4: return this.dados.get(linha).getDescricao();
		case 5: return this.dados.get(linha).getValor();
		
		}
		return null;
	}
	public Servicos getRowAt(int linha) {
		return this.dados.get(linha);
	}	
    public ArrayList<Servicos> getServicos() {
		
		return this.dados;
	}

}
