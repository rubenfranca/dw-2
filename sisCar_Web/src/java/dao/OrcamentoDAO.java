package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import classes.Orcamentos;

import util.ConnectionDAO;
import util.DAO;

public class OrcamentoDAO implements DAO{

	private Connection conn;
	private int lastID;
	private String nome;

	@Override
	public void atualizar(Object ob) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void excluir(Object ob) throws Exception {
		PreparedStatement ps = null;
		Orcamentos orcamento = (Orcamentos) ob;
		try{
			String SQL = "DELETE FROM siscar.pecasorcadas "
					+ "WHERE siscar.pecasorcadas.id_orcamento = ?; "
					+ "DELETE FROM siscar.servicosorcados "
					+ "WHERE siscar.servicosorcados.id_orcamento = ?; "
					+ "DELETE FROM siscar.orcamento "
					+ "WHERE siscar.orcamento.idorcamento = ?;";
			conn = ConnectionDAO.getConnection();
			ps = conn.prepareStatement(SQL);
			ps.setInt(1,orcamento.getIdOrcamento());
			ps.setInt(2,orcamento.getIdOrcamento());
			ps.setInt(3,orcamento.getIdOrcamento());
			ps.executeUpdate();


		}catch (SQLException sqle){
			throw new Exception("Erro ao remover um or�amento!: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
	}

	@Override
	public List listaTodos() throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Orcamentos orc = null;
		ArrayList<Orcamentos> orcamento = new ArrayList<>();
		try{
			String SQL = "select * from siscar.usuario"
					+ "	inner join siscar.orcamento"
					+ "	on siscar.usuario.idusuario = siscar.orcamento.usuario_idusuario;";
			conn = ConnectionDAO.getConnection();
			ps = conn.prepareStatement(SQL);
			rs = ps.executeQuery();
			while(rs.next()) {
				orcamento.add(new Orcamentos(rs.getInt(8),rs.getInt(9),rs.getInt(9)));
			}
		}catch (SQLException sqle){
			throw new Exception("Erro ao Carregar dados: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
		return orcamento;
	}
	public String nome(){
		return this.nome;
	}

	@Override
	public List procura(Object ob) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void salvar(Object ob) throws Exception {
		Orcamentos orcamento = (Orcamentos) ob;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = ConnectionDAO.getConnection();
			String SQL = "INSERT INTO siscar.orcamento(agenda_idagenda, usuario_idusuario) VALUES (?, ?);";
			//ps = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
			ps = conn.prepareStatement(SQL, java.sql.Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, orcamento.getIdAgenda());
			ps.setInt(2, orcamento.getIdUsuario());
			//tratando da inserção e coletando último id;

			int affectedRows = ps.executeUpdate();

			if (affectedRows == 0) 
				throw new SQLException();

			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				this.lastID = rs.getInt(1);
			}else{
				this.lastID = 0;
			}
		}catch (SQLException sqle){
			throw new Exception("Erro ao salvar dados do curso: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}

	}	
	public int getLastID() {
		return this.lastID;
	}

}
