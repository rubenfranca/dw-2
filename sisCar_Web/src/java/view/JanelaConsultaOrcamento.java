package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import classes.Orcamentos;
import dao.PecasOrcadasDAO;
import model.PecaTableModel;
import classes.Pecas;

import javax.swing.*;

import dao.OrcamentoDAO;
import model.OrcamentoTableModel;

public class JanelaConsultaOrcamento extends JFrame{

	private JTable tabelaOrcamento;
	private OrcamentoDAO orcaBanco;
	private PecasOrcadasDAO pecaBanco;
	private int idOrca;
	private JanelaPrincipal janelaPrincipal;

	public JanelaConsultaOrcamento() {
		super("Tela de consulta de orcamento "+"Siscar v1.0");
		setSize(500,500);
		setLocationRelativeTo(null);
		JPanel painel = new JPanel();
		JPanel painelSuperior = new JPanel(new GridLayout(2, 3));

		//botoes
		JButton bMostrar = new JButton("Mostrar");
		painelSuperior.add(bMostrar);
		JButton bExcluir = new JButton("Excluir");
		painelSuperior.add(bExcluir);
		//tabela
		tabelaOrcamento = new JTable();
		JScrollPane barraRolagem = new JScrollPane(tabelaOrcamento);
		JPanel painelCentral = new JPanel(new BorderLayout());
		OrcamentoTableModel orcaModel = new OrcamentoTableModel();
		tabelaOrcamento.setModel(orcaModel);
		painelCentral.add(barraRolagem);
		listarTodos();

		//acoes
		bMostrar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				selecaoOrcamento();			

			}
		});
		bExcluir.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				excluirDados();

			}
		});

		painel.add(painelSuperior);
		painel.add(painelCentral);
		setContentPane(painel);



	}
	public JanelaConsultaOrcamento(JanelaPrincipal janelaPrincipal) {
		super("Tela de consulta");
		this.janelaPrincipal = janelaPrincipal;
		montaTela();
	}
	public void listarTodos(){
		List<Orcamentos> lista;
		orcaBanco = new OrcamentoDAO();
		try {
			lista = orcaBanco.listaTodos();
			OrcamentoTableModel orcaModel = new OrcamentoTableModel();
			for (Orcamentos o: lista){
				orcaModel.addRow(o);
			}
			tabelaOrcamento.setModel(orcaModel);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void selecaoOrcamento(){
		OrcamentoTableModel modelo = (OrcamentoTableModel) tabelaOrcamento.getModel();
		int id = tabelaOrcamento.getSelectedRow();
		if(id==-1){
			JOptionPane.showMessageDialog(null, "Por favor selecione um orcamento!");
		}else{
			Orcamentos orcamento = modelo.getRowAt(id);
			JanelaMostraOrcamento janela = new JanelaMostraOrcamento(orcamento.getIdOrcamento(),this.janelaPrincipal);
			janela.setVisible(true);
			setVisible(false);
		}
	}
	public void excluirDados(){
		try {
			OrcamentoTableModel modelo = (OrcamentoTableModel) tabelaOrcamento.getModel();
			int id = tabelaOrcamento.getSelectedRow();
			
			if(id==-1){
				JOptionPane.showMessageDialog(null, "Por favor selecione um orcamento!");
			}else{
				Orcamentos orcamento = modelo.getRowAt(id);
				orcaBanco.excluir(orcamento);
				modelo.removeRow(tabelaOrcamento.getSelectedRow());
				JOptionPane.showMessageDialog(null,"Excluido com sucesso!");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void montaTela(){
		setSize(500,500);
		setLocationRelativeTo(null);
		JPanel painel = new JPanel();
		JPanel painelSuperior = new JPanel(new GridLayout(2, 3));

		//botoes
		JButton bMostrar = new JButton("Mostrar");
		painelSuperior.add(bMostrar);
		JButton bExcluir = new JButton("Excluir");
		painelSuperior.add(bExcluir);
		//tabela
		tabelaOrcamento = new JTable();
		JScrollPane barraRolagem = new JScrollPane(tabelaOrcamento);
		JPanel painelCentral = new JPanel(new BorderLayout());
		OrcamentoTableModel orcaModel = new OrcamentoTableModel();
		tabelaOrcamento.setModel(orcaModel);
		painelCentral.add(barraRolagem);
		listarTodos();

		//acoes
		bMostrar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				selecaoOrcamento();			

			}
		});
		bExcluir.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				excluirDados();

			}
		});

		painel.add(painelSuperior);
		painel.add(painelCentral);
		setContentPane(painel);
	}
}
