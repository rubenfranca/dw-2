package classes;

public class TipoServico {
	
	private int idTipoServico;
	private String nome;
	private String descricao;
	
	

	public TipoServico(int idTipoServico, String nome, String descricao){
		setIdTipoServico(idTipoServico);
		setDescricao(descricao);
		setNome(nome);
	}
	public int getIdTipoServico() {
		return idTipoServico;
	}

	public void setIdTipoServico(int idTipoServico) {
		this.idTipoServico = idTipoServico;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String toString(){
		return getNome();
	}
}
