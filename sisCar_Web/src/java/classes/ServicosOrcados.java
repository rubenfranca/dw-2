package classes;

public class ServicosOrcados {

	private int id;
	private int idServico;
	private int idOrcamento;
	
	public ServicosOrcados(int id, int idServico, int idOrcamento) {
		super();
		this.id = id;
		this.idServico = idServico;
		this.idOrcamento = idOrcamento;
	}

	public int getId() {
		return id;
	}

	public int getIdServico() {
		return idServico;
	}

	public int getIdOrcamento() {
		return idOrcamento;
	}
}
