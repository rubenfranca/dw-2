function peca(){
    window.location.href = "http://localhost:8080/SiscarWeb/cms/pecas.jsp";
    carregaDados();
}
function servico(){
    window.location.href = "http://localhost:8080/SiscarWeb/cms/servicos.jsp";
}
function orcamento(){
    window.location.href = "http://localhost:8080/SiscarWeb/cms/orcamentos.jsp";
}
$(document).ready(function () {
    carregaDados();
});
function carregaDados() {
    $.ajax({
        url: "pecas"
    }).done(function (data) {
        dados = JSON.parse(data);
        count = 1;
        $.each(dados.data, function (i, element) {
            var idNome = 'colNm' + count;
            var idNome1 = 'colNmDesc' + count;
            var idLinha = 'Linha'+count;
            //alert(element.idProd+"|||"+element.Nome+"|||"+element.idForn+"|||"+element.catProd+"|||"+element.prodDesc);
            var x = document.getElementById("corpoTabela");
            var prev = x.innerHTML;
            x.innerHTML = prev + "<tr id='"+idLinha+"'><td>"+element.idPeca+"</td><td>"+element.Nome+"</td>\n\
                                <td>"+element.descPeca+"</td><td>"+element.quantPeca+"</td>\n\
                                <td>"+element.valorPeca+"</td>\n\
                                <td onClick='excluir("+element.idProd+",\""+idLinha+"\")'>x</td></tr>";
            count++;
        });
    });
}
function excluir(idPeca,idLinha){
    $.ajax({
        url: "pecas?acao=excluir",
        data: {"idPeca": idPeca}
    }).done(function (data) {
        alert(data);
        //carregaDados();
        document.getElementById(idLinha).remove();
    });
}