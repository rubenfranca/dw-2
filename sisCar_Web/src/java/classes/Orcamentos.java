package classes;

public class Orcamentos {
	private int idOrcamento;
	private int idAgenda;
	private int idUsuario;
	private String nomeUsuario;
	private int idItem;
	private String nome;
	private String descricao;
	private float valor;
	private int quant;
	
	public Orcamentos(int id , int idAgenda , int idUsuario){
		this.idOrcamento=id;
		this.idAgenda=idAgenda;
		this.idUsuario=idUsuario;
	}
	public Orcamentos(int idItem , String nome , String descricao, float valor , int quant){
		this.idItem = idItem;
		this.nome = nome;
		this.descricao = descricao;
		this.valor = valor;
		this.quant = quant;
		
	}
	public int getIdOrcamento() {
		return idOrcamento;
	}
	public int getIdAgenda() {
		return idAgenda;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdOrcamento(int idOrcamento) {
		this.idOrcamento = idOrcamento;
	}
	public String getNomeUsuario() {
		return nomeUsuario;
	}
	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}
	public int getIdItem() {
		return idItem;
	}
	public String getNome() {
		return nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public float getValor() {
		return valor;
	}
	public int getQuant() {
		return quant;
	}
	public void setQuant(int quant) {
		this.quant = quant;
	}

}
