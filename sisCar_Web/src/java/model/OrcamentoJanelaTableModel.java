package model;

import classes.Orcamentos;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

public class OrcamentoJanelaTableModel extends AbstractTableModel {
	
	private ArrayList<Orcamentos> dados;
	private String colunas[] = {"  ID  ", "Nome ", "Descricao", "Valor","Quantidade"};
	
	public OrcamentoJanelaTableModel(){
		this.dados = new ArrayList<Orcamentos>();
	}
	public void addRow(Orcamentos o){
		this.dados.add(o);
		this.fireTableDataChanged();
	}
	public void removeRow(int linha) {
		this.dados.remove(linha);
		this.fireTableRowsDeleted(linha, linha);
	}
	public String getColumnName(int col) {
		return this.colunas[col];
	}
	public int tamanho(){
		return this.dados.size();
	}
	@Override
	public int getColumnCount() {
		return this.colunas.length;
	}
	@Override
	public int getRowCount() {
		return this.dados.size();
	}

	@Override
	public Object getValueAt(int linha, int coluna) {
		switch(coluna) {
		case 0: return this.dados.get(linha).getIdItem();
		case 1: return this.dados.get(linha).getNome();
		case 2: return this.dados.get(linha).getDescricao();
		case 3: return this.dados.get(linha).getValor();
		case 4: return this.dados.get(linha).getQuant();
		
		}
		return null;
	}
	public Orcamentos getRowAt(int linha) {
		return this.dados.get(linha);
	}	
    public ArrayList<Orcamentos> getOrcamentos() {
    	return this.dados;
	}

}