package model;

import classes.TipoServico;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class TipoServicoTableModel extends AbstractTableModel{

		
		private ArrayList<TipoServico> dados;
		private String colunas[] = {"ID ","Nome", "Descricao"};
		
		public TipoServicoTableModel(){
			this.dados = new ArrayList<TipoServico>();
		}
		public void addRow(TipoServico s){
			this.dados.add(s);
			this.fireTableDataChanged();
		}
		public void removeRow(int linha) {
			this.dados.remove(linha);
			this.fireTableRowsDeleted(linha, linha);
		}
		public String getColumnName(int col) {
			return this.colunas[col];
		}
		@Override
		public int getColumnCount() {
			return this.colunas.length;
		}
		@Override
		public int getRowCount() {
			return this.dados.size();
		}

		@Override
		public Object getValueAt(int linha, int coluna) {
			switch(coluna) {
			case 0: return this.dados.get(linha).getIdTipoServico();
			case 1: return this.dados.get(linha).getNome();
			case 2: return this.dados.get(linha).getDescricao();
			
			}
			return null;
		}
		public TipoServico getRowAt(int linha) {
			return this.dados.get(linha);
		}	
	    public ArrayList<TipoServico> getTipoServico() {
			
			return this.dados;
		}

	}

