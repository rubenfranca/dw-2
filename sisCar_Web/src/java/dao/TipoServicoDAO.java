package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import classes.TipoServico;

import util.ConnectionDAO;
import util.DAO;

public class TipoServicoDAO implements DAO{

	private Connection conn;
	
	@Override
	public void atualizar(Object ob) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void excluir(Object ob) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List listaTodos() throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<TipoServico> tipoServico = new ArrayList<>();

		try{
			String SQL = "SELECT * FROM siscar.TipoServico";
			conn = ConnectionDAO.getConnection();
			ps = conn.prepareStatement(SQL);
			rs = ps.executeQuery();
			while(rs.next()) {
				tipoServico.add(new TipoServico(rs.getInt(1),rs.getString(2),rs.getString(3)));
			}
		}catch (SQLException sqle){
			throw new Exception("Erro ao Carregar dados: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
		return tipoServico;
	}

	@Override
	public List procura(Object ob) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void salvar(Object ob) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public List listaTodosComIndice() throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<TipoServico> tipoServico = new ArrayList<>();

		try{
			String SQL = //"CREATE INDEX idTipoServico ON  siscar.TipoServico (idTipoServico); "
					 "SELECT * FROM siscar.TipoServico";
			conn = ConnectionDAO.getConnection();
			ps = conn.prepareStatement(SQL);
			rs = ps.executeQuery();
			while(rs.next()) {
				tipoServico.add(new TipoServico(rs.getInt(1),rs.getString(2),rs.getString(3)));
			}
		}catch (SQLException sqle){
			throw new Exception("Erro ao Carregar dados: \n"+sqle);
			
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
			//conn.rollback();
		}
		return tipoServico;
	}

}
