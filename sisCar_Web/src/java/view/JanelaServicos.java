package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.*;

import model.ServicoComboBoxModel;
import dao.ServicoDAO;
import model.ServicoTableModel;
import classes.Servicos;
import classes.TipoServico;
import model.TipoServicoComboBoxModel;
import dao.TipoServicoDAO;

public class JanelaServicos extends JFrame{
	
	private JTable tabelaServico;
	private Servicos servicoSelecionado = null;
	private JTextField tDescricao;
	private JTextField tValor;
	private JTextField tTempo;
	private ServicoDAO servicoBanco;
	private JComboBox tTipoServico;
	private TipoServicoDAO tipoServicoBanco;
	private JTextField tNomeServico;

	public JanelaServicos(){
		super("Tela de Servicos");
		setSize(500,500);		
		JPanel painel = new JPanel(new BorderLayout());
		JPanel painelSuperior = new JPanel(new GridLayout(0,2,0,0));
		setLocationRelativeTo(null);
		JToolBar bar = new JToolBar();
		bar.setFloatable(false);




		//botoes		
		JButton bSalvar = new JButton("Salvar");
		bar.add(bSalvar);

		JButton bExcluir = new JButton("Excluir");
		bar.add(bExcluir);
			
		bar.addSeparator();

		JButton bVoltar = new JButton("Voltar");
		bar.add(bVoltar);


		//labels
		JLabel lNomeServico = new JLabel("Nome Servico");
		JLabel lTipoServico = new JLabel("Tipo Servico");
		JLabel lDescricao = new JLabel("Descri��o");
		JLabel lTempo = new JLabel("Tempo");
		JLabel lValor = new JLabel("Valor");


		//campos de texto
		tNomeServico = new JTextField();
		tTipoServico = new JComboBox();
		tDescricao = new JTextField();
		tValor = new JTextField();
		tTempo = new JTextField();

		//adicao no painel
		painelSuperior.add(lNomeServico);
		painelSuperior.add(tNomeServico);
		painelSuperior.add(lTipoServico);
		painelSuperior.add(tTipoServico);
		painelSuperior.add(lDescricao);
		painelSuperior.add(tDescricao);
		painelSuperior.add(lValor);
		painelSuperior.add(tValor);
		painelSuperior.add(lTempo);
		painelSuperior.add(tTempo);

		//tabela
		tabelaServico = new JTable();
		JScrollPane barraRolagem = new JScrollPane(tabelaServico);
		JPanel painelCentral = new JPanel(new BorderLayout());
		ServicoTableModel servicoModel = new ServicoTableModel();
		tabelaServico.setModel(servicoModel);
		painelCentral.add("Center",barraRolagem);
		painelCentral.add("North",bar);
		buscarDados();
		preencheComboBoxServico();


		//acoes aos botoes
		bVoltar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				voltar();

			}
		});
		bExcluir.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				excluirDados();
				
			}
		});
		bSalvar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cadastroDados();

			}
		});
		tabelaServico.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount()>1) {
					preencheCampos();
				}

			}
		});

		//adicoes no painel principal
		painel.add("Center",painelCentral);
		painel.add("North",painelSuperior);
		setContentPane(painel);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);


	}
	public void excluirDados()  {
		ServicoTableModel modelo = (ServicoTableModel) tabelaServico.getModel();
		int id = tabelaServico.getSelectedRow();
		Servicos servico = modelo.getRowAt(id);

		try {
			servicoBanco.excluir(servico);
			modelo.removeRow(tabelaServico.getSelectedRow());
			JOptionPane.showMessageDialog(null, "Deletado com sucesso");
			limparDados();
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public List buscarDados() {
		try {
			List<Servicos> lista;
			servicoBanco = new ServicoDAO();
			lista = servicoBanco.listaTodos();
			ServicoTableModel pModel = new  ServicoTableModel();

			for(Servicos s : lista) {

				pModel.addRow(s);

			}
			tabelaServico.setModel(pModel);
			return lista;
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null, "problema ao carregar dados!"+e.getMessage());
		}
		return null;
	}
	public void cadastroDados(){
		TipoServico sv= null;
		sv =  (TipoServico) tTipoServico.getSelectedItem();
		if(servicoSelecionado == null) {			
			Servicos servico = new Servicos(0,tNomeServico.getText(),""+sv.getIdTipoServico(),Integer.parseInt(tTempo.getText()),tDescricao.getText(),Float.parseFloat(tValor.getText()));
			ServicoDAO conServico;
			ServicoTableModel modelo = (ServicoTableModel) tabelaServico.getModel();

			try {
				conServico = new ServicoDAO();
				conServico.salvar(servico);
				modelo.addRow(new Servicos(conServico.getLastID(),tNomeServico.getText(),sv.getNome(),Integer.parseInt(tTempo.getText()),tDescricao.getText(),Float.parseFloat(tValor.getText())));
				JOptionPane.showMessageDialog(null, "Cadastrado com sucesso!");
				limparDados();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			Servicos servico = new Servicos(servicoSelecionado.getIdServicos(),tNomeServico.getText(),sv.getNome(),Integer.parseInt(tTempo.getText()),tDescricao.getText(),Float.parseFloat(tValor.getText()));
			salvarDadosAlterados(servico);
			ServicoTableModel modelo = (ServicoTableModel) tabelaServico.getModel();
			modelo.removeRow(tabelaServico.getSelectedRow());
			modelo.addRow(servico);
			JOptionPane.showMessageDialog(null, "Alterado com sucesso!");
			limparDados();
		}
	}
	public void voltar(){
		this.dispose();
	}
	public void limparDados(){
		tNomeServico.setText("");
		tTipoServico.setSelectedItem(null);
		tDescricao.setText("");
		tValor.setText("");
		tTempo.setText("");
		servicoSelecionado=null;
	}
	public void salvarDadosAlterados(Servicos s){
		try {
			servicoBanco.atualizar(s);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public void preencheCampos() {
		ServicoTableModel modelo = (ServicoTableModel) tabelaServico.getModel();
		int id = tabelaServico.getSelectedRow();
		servicoSelecionado = modelo.getRowAt(id);
		tNomeServico.setText(servicoSelecionado.getNome());
		tTipoServico.setSelectedItem(servicoSelecionado.getTipoServico());
		tDescricao.setText(servicoSelecionado.getDescricao());
		tValor.setText(""+servicoSelecionado.getValor());
		tTempo.setText(""+servicoSelecionado.getTempoServico());
	}
	public void preencheComboBoxServico() {
		try {
			List<TipoServico> lista;
			tipoServicoBanco = new TipoServicoDAO();
			lista = tipoServicoBanco.listaTodos();
			TipoServicoComboBoxModel modelo = new TipoServicoComboBoxModel();
			for(TipoServico s: lista) {
				modelo.addItem(s);
			}
			tTipoServico.setModel(modelo);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "problema ao carregar dados2!"+e.getMessage());
		}
	}

}
