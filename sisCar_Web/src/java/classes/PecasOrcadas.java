package classes;

public class PecasOrcadas {
	private int id;
	private int idPecas;
	private int idOrcamento;
	
	public PecasOrcadas(int id, int idPecas,int idOrcamento){
		this.id=id;
		this.idPecas=idPecas;
		this.idOrcamento=idOrcamento;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdPecas() {
		return idPecas;
	}

	public int getIdOrcamento() {
		return idOrcamento;
	}
}
