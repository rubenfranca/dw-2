package classes;

public class Servicos {

	private int idServicos;
	private String tipoServico;
	private int tempoServico;
	private String descricao;
	private float valor;
	private String nome;

	public Servicos(int idServicos, String tipoServico, int tempoServico, String descricao, float valor) {
		this.idServicos = idServicos;
		this.tipoServico = tipoServico;
		this.tempoServico = tempoServico;
		this.descricao = descricao;
		this.valor = valor;
	}
	public Servicos(int idServicos, String nome,String tipoServico, int tempoServico, String descricao, float valor) {
		this.nome = nome;
		this.idServicos = idServicos;
		this.tipoServico = tipoServico;
		this.tempoServico = tempoServico;
		this.descricao = descricao;
		this.valor = valor;
	}

	public int getIdServicos() {
		return idServicos;
	}
	public String getTipoServico() {
		return tipoServico;
	}
	public int getTempoServico() {
		return tempoServico;
	}
	public String getDescricao() {
		return descricao;
	}
	public float getValor() {
		return valor;
	}
	public String getNome(){
		return this.nome;
	}
	public String toString(){
		return descricao;
	}
}
