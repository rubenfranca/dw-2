package model;

import classes.TipoServico;
import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class TipoServicoComboBoxModel extends AbstractListModel implements ComboBoxModel{

	
	private ArrayList<TipoServico> dados;
	private TipoServico selection;
	
	
	public TipoServicoComboBoxModel() {
		
		this.dados = new ArrayList<TipoServico>();
	}
	
	public void addItem(TipoServico c) {
		this.dados.add(c);
	}
	
	@Override
	public int getSize() {
		return this.dados.size();				
	}
	
    public ArrayList<TipoServico> getCurso() {
    	
		return this.dados;
	}

	@Override
	public Object getElementAt(int index) {
		return this.dados.get(index);
	}
	

	@Override
	public Object getSelectedItem() {
		return selection;
	}

	@Override
	public void setSelectedItem(Object anItem) {
		selection = (TipoServico) anItem;		
	}


}
