package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import util.ConnectionDAO;
import javax.swing.*;

import dao.PecaDAO;
import model.PecaTableModel;
import classes.Pecas;

public class JanelaPecas extends JFrame{
	private JTextField tPecas;
	private JTable tabelaPeca;
	private Pecas pecaSelecionada = null;
	private JTextField tNome;
	private JTextField tDescricao;
	private JTextField tValor;
	private JTextField tQuant;
	private PecaDAO pecaBanco;

	public JanelaPecas(){
		super("Tela de Pecas");
		setSize(500,500);		
		JPanel painel = new JPanel(new BorderLayout());
		JPanel painelSuperior = new JPanel(new GridLayout(0,2,0,0));
		setLocationRelativeTo(null);
		JToolBar bar = new JToolBar();
		bar.setFloatable(false);




		//botoes		
		JButton bSalvar = new JButton("Salvar");
		bar.add(bSalvar);

		JButton bExcluir = new JButton("Excluir");
		bar.add(bExcluir);



		bar.addSeparator();

		JButton bVoltar = new JButton("Voltar");
		bar.add(bVoltar);


		//labels
		JLabel lNmPeca = new JLabel("Nome da pe�a");
		JLabel lDescricao = new JLabel("Descri��o");
		JLabel lQuant = new JLabel("Quantidade");
		JLabel lValor = new JLabel("Valor");


		//campos de texto
		tNome = new JTextField();
		tDescricao = new JTextField();
		tValor = new JTextField();
		tQuant = new JTextField();

		//adicao no painel
		painelSuperior.add(lNmPeca);
		painelSuperior.add(tNome);
		painelSuperior.add(lDescricao);
		painelSuperior.add(tDescricao);
		painelSuperior.add(lValor);
		painelSuperior.add(tValor);
		painelSuperior.add(lQuant);
		painelSuperior.add(tQuant);

		//tabela
		tabelaPeca = new JTable();
		JScrollPane barraRolagem = new JScrollPane(tabelaPeca);
		JPanel painelCentral = new JPanel(new BorderLayout());
		PecaTableModel pecaModel = new PecaTableModel();
		tabelaPeca.setModel(pecaModel);
		painelCentral.add("Center",barraRolagem);
		painelCentral.add("North",bar);
		buscarDados();


		//acoes aos botoes
		bVoltar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				voltar();

			}
		});
		bExcluir.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				excluirDados();

			}
		});
		bSalvar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cadastroPeca();

			}
		});
		tabelaPeca.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount()>1) {
					preencheCampos();
				}

			}
		});

		//adicoes no painel principal
		painel.add("Center",painelCentral);
		painel.add("North",painelSuperior);
		setContentPane(painel);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);


	}
	public void excluirDados()  {
		PecaTableModel modelo = (PecaTableModel) tabelaPeca.getModel();
		int id = tabelaPeca.getSelectedRow();
		Pecas pecas= modelo.getRowAt(id);

		try {
			pecaBanco.excluir(pecas);
			modelo.removeRow(tabelaPeca.getSelectedRow());
			JOptionPane.showMessageDialog(null, "Deletado com sucesso");
			limparDados();
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public List buscarDados() {
		try {
			List<Pecas> lista;
			pecaBanco = new PecaDAO();
			lista = pecaBanco.listaTodos();
			PecaTableModel pModel = new  PecaTableModel();

			for(Pecas p : lista) {

				pModel.addRow(p);

			}
			tabelaPeca.setModel(pModel);
			return lista;
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null, "problema ao carregar dados!"+e.getMessage());
		}
		return null;
	}
	public void cadastroPeca(){
		if(pecaSelecionada == null) {
			Pecas peca = new Pecas(0,tNome.getText(),tDescricao.getText(),Float.parseFloat(tValor.getText()),Integer.parseInt(tQuant.getText()));
			PecaDAO conPeca;
			PecaTableModel modelo = (PecaTableModel) tabelaPeca.getModel();

			try {
				conPeca = new PecaDAO();
				conPeca.salvar(peca);
				modelo.addRow(new Pecas(conPeca.getLastID(),tNome.getText(),tDescricao.getText(),Float.parseFloat(tValor.getText()),Integer.parseInt(tQuant.getText())));
				limparDados();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			Pecas peca = new Pecas(pecaSelecionada.getIdPecas(),tNome.getText(),tDescricao.getText(),Float.parseFloat(tValor.getText()),Integer.parseInt(tQuant.getText()));
			salvarDadosAlterados(peca);
			PecaTableModel modelo = (PecaTableModel) tabelaPeca.getModel();
			modelo.removeRow(tabelaPeca.getSelectedRow());
			modelo.addRow(peca);
			limparDados();
		}
	}
	public void voltar(){
		this.dispose();
	}
	public void limparDados(){
		tPecas.setText("");
		tNome.setText("");
		tDescricao.setText("");
		tValor.setText("");
		tQuant.setText("");
		pecaSelecionada=null;
	}
	public void salvarDadosAlterados(Pecas p){
		try {
			pecaBanco.atualizar(p);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public void preencheCampos() {
		PecaTableModel modelo = (PecaTableModel) tabelaPeca.getModel();
		int id = tabelaPeca.getSelectedRow();
		pecaSelecionada = modelo.getRowAt(id);
		tNome.setText(pecaSelecionada.getNome());
		tDescricao.setText(pecaSelecionada.getDescricao());
		tValor.setText(""+pecaSelecionada.getValor());
		tQuant.setText(""+pecaSelecionada.getQuant());
	}

}
