package controlador;

import util.ConnectionDAO;
import view.JanelaPrincipal;

public class Controlador {
	public static void main(String[] args) throws Exception {
		ConnectionDAO.getConnection();
		JanelaPrincipal janela = new JanelaPrincipal();
		janela.setVisible(true);
	}
}
