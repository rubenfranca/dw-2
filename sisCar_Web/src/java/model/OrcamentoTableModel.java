package model;

import classes.Orcamentos;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

public class OrcamentoTableModel extends AbstractTableModel{

	private ArrayList<Orcamentos> dados;
	private String colunas[] = {"id usuario","Id Or�amento"};

	public OrcamentoTableModel(){
		this.dados = new ArrayList<Orcamentos>();
	}
	public void addRow(Orcamentos o){
		this.dados.add(o);
		this.fireTableDataChanged();
	}
	public void removeRow(int linha) {
		this.dados.remove(linha);
		this.fireTableRowsDeleted(linha, linha);
	}
	public String getColumnName(int col) {
		return this.colunas[col];
	}
	@Override
	public int getColumnCount() {
		return this.colunas.length;
	}
	@Override
	public int getRowCount() {
		return this.dados.size();
	}

	@Override
	public Object getValueAt(int linha, int coluna) {
		switch(coluna) {
		case 0: return this.dados.get(linha).getIdUsuario();
		case 1: return this.dados.get(linha).getIdOrcamento();
		}
		return null;
	}
	public Orcamentos getRowAt(int linha) {
		return this.dados.get(linha);
	}	
	public ArrayList<Orcamentos> getCurso() {

		return this.dados;
	}

}
