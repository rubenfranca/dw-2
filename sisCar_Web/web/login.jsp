<%-- 
    Document   : login.jsp
    Created on : 20/11/2018, 14:22:47
    Author     : Usuário
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/login.css">
        <title>Siscar Web 1.0</title>
    </head>
    <body>
        <div id="fundoDoLogin" class="container">

            <div class="box">
                <h1 id="titulo1">Bem vindo ao SisCar</h1>
                
                <label id="labelId">Digite seu login</label>
                <input id="idLogin" type="text" value=""></input>
                
                <label id="labelSenha">Digite sua senha</label>
                <input id="idSenha" type="password" value=""></input>
                
                <button id="botaoLogin" onclick="logar()" type="button">Acessar</button>
            </div>
            <br>
            

        </div>
    </body>
    <script src="js/jquery.js"></script>
    <script src="js/principal.js"></script>

</html>
