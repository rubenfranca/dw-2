package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import classes.Pecas;
import classes.PecasOrcadas;

import util.ConnectionDAO;
import util.DAO;

public class PecasOrcadasDAO implements DAO {

	private Connection conn;
	private int lastID;
	@Override
	public void atualizar(Object ob) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void excluir(Object ob) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List listaTodos() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List procura(Object ob) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Pecas> pecas = new ArrayList<>();
		try{
			String SQL = "select * from siscar.pecas "
					+ "inner join siscar.pecasorcadas "
					+ "on siscar.pecasorcadas.id_pecas = siscar.pecas.idpecas "
					+ "where siscar.pecasorcadas.id_orcamento = ?";
			ps = ConnectionDAO.getConnection().prepareStatement(SQL);
			ps.setInt(1, Integer.parseInt((String) ob));
			rs = ps.executeQuery();
			while(rs.next()){
				pecas.add(new Pecas(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getFloat(4),1));
			}

		}catch (SQLException sqle){
			throw new Exception("Erro ao carregar pecas: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
		return pecas;
	}
	public void salvar(Object ob,Pecas peca) throws Exception {
		PecasOrcadas pecas= (PecasOrcadas) ob;
		Pecas selecao = peca;
		PreparedStatement ps = null;
		PreparedStatement psQuant = null;
		ResultSet rs = null;
		Connection conn = null;
		try{
			conn = ConnectionDAO.getConnection();
			
			String atualizarQuant = "UPDATE siscar.pecas SET quantidade = ";
			psQuant = conn.prepareStatement(atualizarQuant);
			psQuant.setInt(1, selecao.getQuant()-1);
					
			String SQL = "INSERT INTO siscar.pecasorcadas(id_pecas, id_orcamento) VALUES ( ?, ?);";
			ps = conn.prepareStatement(SQL, java.sql.Statement.RETURN_GENERATED_KEYS);		
			ps.setInt(1, pecas.getIdPecas());
			ps.setInt(2, pecas.getIdOrcamento());
			//tratando da inserção e coletando último id;
			
			psQuant.executeUpdate();
			ps.executeUpdate();
			conn.commit();
			
			rs = ps.getGeneratedKeys();
			
			
			if (rs.next()) {
				this.lastID = rs.getInt(1);
			}else{
				this.lastID = 0;
			}

		}catch (SQLException sqle){
			if(conn != null){
				try{
					conn.setAutoCommit(false);
					System.err.print("Falha transacao em Rollback!!!!");
					conn.rollback();
				}catch(SQLException sqlE){
					sqlE.printStackTrace();
				}
			}
			throw new Exception("Erro ao salvar dados!: \n"+sqle);
		}finally{
			conn.setAutoCommit(true);
			ConnectionDAO.closeConnection(conn,ps);
		}

	}	
	public int getLastID() {
		return this.lastID;
	}
	public void salvar(Object ob) throws Exception {
		
	}

}
