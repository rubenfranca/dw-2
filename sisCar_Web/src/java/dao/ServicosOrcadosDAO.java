package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import classes.Servicos;
import classes.ServicosOrcados;

import util.ConnectionDAO;
import util.DAO;

public class ServicosOrcadosDAO implements DAO{

	private int lastID;
	private Connection conn;
	@Override
	public void atualizar(Object ob) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void excluir(Object ob) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List listaTodos() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List procura(Object ob) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Servicos> servicos = new ArrayList<>();
		try{
			String SQL = "select * from siscar.servicos "
					+ "inner join siscar.servicosorcados "
					+ "on siscar.servicosorcados.id_servicos = siscar.servicos.idservicos "
					+"inner join siscar.tiposervico "
					+ "on siscar.servicos.tiposervico_idtiposervico = siscar.tiposervico.idtiposervico "
					+ "where siscar.servicosorcados.id_orcamento = ?;";
			ps = ConnectionDAO.getConnection().prepareStatement(SQL);
			ps.setInt(1, Integer.parseInt((String) ob));
			rs = ps.executeQuery();
			while(rs.next()){
				servicos.add(new Servicos(rs.getInt(1),rs.getString(2),rs. getString(11),rs.getInt(4),rs.getString(5),rs.getFloat(6)));
			}

		}catch (SQLException sqle){
			throw new Exception("Erro ao carregar pecas: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
		return servicos;
	}
	@Override
	public void salvar(Object ob) throws Exception {
		ServicosOrcados servico= (ServicosOrcados) ob;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = ConnectionDAO.getConnection();
			String SQL = "INSERT INTO siscar.servicosorcados(id_servicos, id_orcamento) VALUES ( ?, ?);";
			ps = conn.prepareStatement(SQL, java.sql.Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, servico.getIdServico());
			ps.setInt(2, servico.getIdOrcamento());
			//tratando da inserção e coletando último id;

			int affectedRows = ps.executeUpdate();

			if (affectedRows == 0) 
				throw new SQLException();

			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				this.lastID = rs.getInt(1);
			}else{
				this.lastID = 0;
			}

		}catch (SQLException sqle){
			throw new Exception("Erro ao salvar dados!: \n"+sqle);
			
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
		
	}
	public int getLastID() {
		return this.lastID;
	}

}
