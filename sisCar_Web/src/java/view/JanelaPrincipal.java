package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class JanelaPrincipal extends JFrame{
	
	public JanelaConsultaOrcamento janela;

	public JanelaPrincipal(){
		super("Siscar v1.0");
		setSize(500,500);
		setLocationRelativeTo(null);
		JPanel painel = new JPanel(new BorderLayout());

		//declaracao dos menus e da barra de menu
		JMenuBar menu = new JMenuBar();
		JMenu servicos = new JMenu("Servicos");
		JMenu pecas = new JMenu("Pecas");
		JMenu orcamento = new JMenu("Orçamentos");		

		//itens do menu
		JMenuItem cadastroPeca = new JMenuItem("Cadastro");
		JMenuItem cadastroServico = new JMenuItem("Cadastro");
		JMenuItem cadastroOrcamento = new JMenuItem("Cadastro");
		JMenuItem consultaPeca = new JMenuItem("Consulta");
		JMenuItem consultaServico = new JMenuItem("Consulta");
		JMenuItem consultaOrcamento = new JMenuItem("Consulta");

		//adicionando os itens nos menus
		pecas.add(cadastroPeca);
		servicos.add(cadastroServico);
		orcamento.add(cadastroOrcamento);
		pecas.add(consultaPeca);
		servicos.add(consultaServico);
		orcamento.add(consultaOrcamento);

		//adicionando os menus na barra
		menu.add(pecas);
		menu.add(servicos);
		menu.add(orcamento);

		//acoes dos itens do menu
		cadastroPeca.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JanelaPecas janela = new JanelaPecas();
				janela.setVisible(true);

			}
		});
		cadastroServico.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JanelaServicos janela = new JanelaServicos();
				janela.setVisible(true);

			}
		});
		cadastroOrcamento.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JanelaOrcamento janela = new JanelaOrcamento();
				janela.setVisible(true);

			}
		});
		consultaOrcamento.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				abreJanelaOrcamento();
				//janela = new JanelaConsultaOrcamento();
				//janela.setVisible(true);
				
			}
		});
		consultaPeca.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JanelaConsultaPecas janela = new JanelaConsultaPecas();
				janela.setVisible(true);
				
			}
		});
		consultaServico.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JanelaConsultaServicos janela = new JanelaConsultaServicos();
				janela.setVisible(true);
				
			}
		});


		setDefaultCloseOperation(EXIT_ON_CLOSE);
		painel.add("North",menu);
		setContentPane(painel);

	}
	public void abreJanelaOrcamento(){
		janela = new JanelaConsultaOrcamento(this);
		janela.setVisible(true);
	}
	public JanelaConsultaOrcamento getJanelaConsulta(){
		return this.janela;
	}
	

}
