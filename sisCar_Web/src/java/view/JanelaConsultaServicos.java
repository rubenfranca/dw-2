package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;

import dao.PecaDAO;
import model.PecaTableModel;
import classes.Pecas;
import classes.PecasOrcadas;
import dao.PecasOrcadasDAO;
import dao.ServicoDAO;
import model.ServicoTableModel;
import classes.Servicos;
import classes.ServicosOrcados;
import dao.ServicosOrcadosDAO;

public class JanelaConsultaServicos extends JFrame{

	private int idOrcamento;
	private JTextField tServicos;
	private JTable tabelaServico;
	private ServicosOrcadosDAO ServicosOrcadosBanco;

	public JanelaConsultaServicos(){
		super("Tela de Pecas");
		setSize(500,500);
		JPanel painel = new JPanel(new BorderLayout());
		JPanel painelSuperior = new JPanel(new GridLayout(0,2,0,0));
		setLocationRelativeTo(null);
		JToolBar bar = new JToolBar();
		bar.setFloatable(false);

		//botoes
		JButton bConsulta = new JButton("Consulta");

		//campos de texto
		tServicos = new JTextField();


		//labels
		JLabel lConsulta = new JLabel("Digite o servico que deseja consultar: ");

		//tabela
		tabelaServico = new JTable();
		JScrollPane barraRolagem = new JScrollPane(tabelaServico);
		JPanel painelCentral = new JPanel(new BorderLayout());
		ServicoTableModel servicoModel = new ServicoTableModel();
		tabelaServico.setModel(servicoModel);
		painelCentral.add("Center",barraRolagem);
		painelCentral.add("North",bar);

		//acoes dos botoes
		bConsulta.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				procuraServicos();

			}
		});


		bar.add(tServicos);
		bar.add(bConsulta);
		painel.add("Center",painelCentral);
		painel.add("North",painelSuperior);
		setContentPane(painel);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

	}
	public void procuraServicos(){

		List<Servicos> lista;
		ServicoTableModel modelo = (ServicoTableModel) tabelaServico.getModel();
		try {
			ServicoDAO cons = new ServicoDAO();
			String aux = tServicos.getText();
			lista = cons.procura(aux);
			if(!lista.isEmpty()){

				for(Servicos s : lista) {

					modelo.addRow(s);

				}
				tabelaServico.setModel(modelo);

			}else{
				JOptionPane.showMessageDialog(null,"Pe�a nao encontrada!");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
