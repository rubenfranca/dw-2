/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cms.cms.controller;

import dao.PecaDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Pecas;
import org.json.simple.JSONObject;
import org.json.simple.JsonArray;

/**
 *
 * @author Usuário
 */
public class PecasServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            JSONObject pai = new JSONObject();
            JsonArray filhos = new JsonArray();
            JSONObject dados;
            List<Pecas> peca = new ArrayList<>();
            String acao = request.getParameter("acao");
            try {
                PecaDAO pecaDados = new PecaDAO();
                if(acao!=null&&acao.equals("excluir")){
                    int idPeca = Integer.parseInt(request.getParameter("idPeca"));
                    pecaDados.excluir(idPeca);
                }else{
                
                peca = pecaDados.listaTodos();
                    for (Pecas p : peca) {

                        dados = new JSONObject();
                        dados.put("Nome", p.getNome());
                        dados.put("idPeca", p.getIdPecas());
                        dados.put("descPeca", p.getDescricao());
                        dados.put("quantPeca", p.getQuant());
                        dados.put("valorPeca", p.getValor());
                        filhos.add(dados);
                    }
                    pai.put("data", filhos);
                    out.println(pai);
                }
            } catch (Exception ex) {
                Logger.getLogger(PecasServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
