package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.*;

import model.OrcamentoTableModel;
import classes.Orcamentos;
import model.PecaTableModel;
import classes.Pecas;
import dao.PecasOrcadasDAO;
import model.ServicoTableModel;
import classes.Servicos;
import dao.ServicosOrcadosDAO;

public class JanelaMostraOrcamento extends JFrame{

	private PecasOrcadasDAO pecaBanco;
	private int idOrca;
	private JTable tabelaPeca;
	private JTable tabelaServico;
	private ServicosOrcadosDAO servicoBanco;
	private JanelaPrincipal janelaPrincipal;

	public JanelaMostraOrcamento(int idOrcamento) {
		super("Descricao Orcamentos " + "   Siscar v1.0");
		this.idOrca  = idOrcamento;		
		setSize(800,800);
		setLocationRelativeTo(null);
		JPanel painel = new JPanel();
		JPanel painelDeCima = new JPanel(new GridLayout(2, 3,0,0));
		
		
		//label
		JLabel lOrcamento = new JLabel("Orcamento Numero: "+this.idOrca+"\n");
		JLabel lPecas = new JLabel("Pecas: ");
		painelDeCima.add(lOrcamento);
		painelDeCima.add(lPecas);
		

		//tabela
		tabelaPeca = new JTable();
		tabelaServico = new JTable();
		JScrollPane barraRolagem2 =  new JScrollPane(tabelaServico);
		JScrollPane barraRolagem = new JScrollPane(tabelaPeca);
		JPanel painelCentral = new JPanel(new BorderLayout());
		PecaTableModel pecaModel = new PecaTableModel();
		ServicoTableModel servicoModel = new ServicoTableModel();
		tabelaPeca.setModel(pecaModel);
		tabelaServico.setModel(servicoModel);
		painelCentral.add("North",painelDeCima);
		painelCentral.add("Center",barraRolagem);
		painelCentral.add("South",barraRolagem2);		
		abreOrcamento();


		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				if (JOptionPane.showConfirmDialog(null,"Deseja sair")==JOptionPane.OK_OPTION){
						}
		}
		});
	
		painel.add(painelCentral);
		setContentPane(painel);
	}	
	public JanelaMostraOrcamento(int idOrcamento, JanelaPrincipal janelaPrincipal) {
		super("Orcamento Numero: "+idOrcamento);
		this.janelaPrincipal = janelaPrincipal;
		this.idOrca  = idOrcamento;
		montaTela();
		
	}
	public void abreOrcamento(){

		try {
			pecaBanco = new PecasOrcadasDAO();
			String id = ""+idOrca;
			List<Pecas> lista;
			lista = pecaBanco.procura(id);
			PecaTableModel modelo = new PecaTableModel();
			for(Pecas p : lista) {

				modelo.addRow(p);

			}
			tabelaPeca.setModel(modelo);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			servicoBanco = new ServicosOrcadosDAO();
			String id = ""+idOrca;
			List<Servicos> lista;
			lista = servicoBanco.procura(id);
			ServicoTableModel modelo1 = new ServicoTableModel();
			for(Servicos c : lista) {

				modelo1.addRow(c);

			}
			tabelaServico.setModel(modelo1);			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public void fecharJanela(){
		janelaPrincipal.janela.setVisible(true);
		dispose();
	}
	public void montaTela(){		
		setSize(800,800);
		setLocationRelativeTo(null);
		JPanel painel = new JPanel();
		JPanel painelDeCima = new JPanel(new GridLayout(2, 3,0,0));
		
		
		//label
		JLabel lOrcamento = new JLabel("Orcamento Numero: "+this.idOrca+"\n");
		JLabel lPecas = new JLabel("Pecas: ");
		painelDeCima.add(lOrcamento);
		painelDeCima.add(lPecas);
		

		//tabela
		tabelaPeca = new JTable();
		tabelaServico = new JTable();
		JScrollPane barraRolagem2 =  new JScrollPane(tabelaServico);
		JScrollPane barraRolagem = new JScrollPane(tabelaPeca);
		JPanel painelCentral = new JPanel(new BorderLayout());
		PecaTableModel pecaModel = new PecaTableModel();
		ServicoTableModel servicoModel = new ServicoTableModel();
		tabelaPeca.setModel(pecaModel);
		tabelaServico.setModel(servicoModel);
		painelCentral.add("North",painelDeCima);
		painelCentral.add("Center",barraRolagem);
		painelCentral.add("South",barraRolagem2);		
		abreOrcamento();


		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				if (JOptionPane.showConfirmDialog(null,"Deseja sair")==JOptionPane.OK_OPTION){
						}
		}
		});
	
		painel.add(painelCentral);
		setContentPane(painel);
	}
}
