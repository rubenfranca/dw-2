/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Usuário
 */
public class LoginModel {
    private int id;
    private String nome;
    private String sobreNome;
    private String setor;
    private String rua;   
    private String senha;
    private String email;
    private int tipoUsuario;

    public LoginModel(int id, String nome, String sobreNome, String setor, String rua, String senha, String email, int tipoUsuario) {
        this.id = id;
        this.nome = nome;
        this.sobreNome = sobreNome;
        this.setor = setor;
        this.rua = rua;
        this.senha = senha;
        this.email = email;
        this.tipoUsuario = tipoUsuario;
    }
    public LoginModel(String email, String senha, int tipoUsuario) {
        this.senha = senha;
        this.email = email;
        this.tipoUsuario = tipoUsuario;
    }

    public LoginModel(String email, String senha) {
        this.senha = senha;
        this.email = email;
    }

    public LoginModel() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobreNome() {
        return sobreNome;
    }

    public void setSobreNome(String sobreNome) {
        this.sobreNome = sobreNome;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(int tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }
    
    
    
}
