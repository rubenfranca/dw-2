package model;

import classes.Pecas;
import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class PecasComboBoxModel extends AbstractListModel implements ComboBoxModel{

	private ArrayList<Pecas> dados;
	private Pecas selection;

	public PecasComboBoxModel() {

		this.dados = new ArrayList<Pecas>();
	}

	public void addItem(Pecas p) {
		this.dados.add(p);
	}

	@Override
	public int getSize() {
		return this.dados.size();				
	}

	public ArrayList<Pecas> getPecas() {

		return this.dados;
	}

	@Override
	public Object getElementAt(int index) {
		return this.dados.get(index);
	}


	@Override
	public Object getSelectedItem() {
		return selection;
	}

	@Override
	public void setSelectedItem(Object anItem) {
		selection = (Pecas) anItem;		
	}

}
