package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;

import classes.Orcamentos;
import dao.PecaDAO;
import model.PecaTableModel;
import classes.Pecas;
import classes.PecasOrcadas;
import dao.PecasOrcadasDAO;

public class JanelaConsultaPecas extends JFrame{


	private JTable tabelaPeca;
	private JTextField tPecas;
	private PecasOrcadas novo;
	private PecasOrcadasDAO pecasOrcadasBanco;
	private Pecas returno;
	
	public JanelaConsultaPecas(){
		super("Tela de Pecas");
		setSize(500,500);
		JPanel painel = new JPanel(new BorderLayout());
		JPanel painelSuperior = new JPanel(new GridLayout(0,2,0,0));
		setLocationRelativeTo(null);
		JToolBar bar = new JToolBar();
		bar.setFloatable(false);

		//botoes
		JButton bConsulta = new JButton("Consulta");

		//campos de texto
		tPecas = new JTextField();
		
		
		//labels
		JLabel lConsulta = new JLabel("Digite a peca que deseja consultar: ");
		
		//tabela
		tabelaPeca = new JTable();
		JScrollPane barraRolagem = new JScrollPane(tabelaPeca);
		JPanel painelCentral = new JPanel(new BorderLayout());
		PecaTableModel pecaModel = new PecaTableModel();
		tabelaPeca.setModel(pecaModel);
		painelCentral.add("Center",barraRolagem);
		painelCentral.add("North",bar);
		
		//acoes dos botoes
		bConsulta.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				procuraPecas();

			}
		});	
		
		bar.add(tPecas);
		bar.add(bConsulta);
		painel.add("Center",painelCentral);
		painel.add("North",painelSuperior);
		setContentPane(painel);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
	}
	public void procuraPecas(){

		List<Pecas> lista;
		PecaTableModel modelo = (PecaTableModel) tabelaPeca.getModel();
		try {
			PecaDAO cons = new PecaDAO();
			String aux = tPecas.getText();
			lista = cons.procura(aux);
			if(!lista.isEmpty()){

				for(Pecas p : lista) {

					modelo.addRow(p);

				}
				tabelaPeca.setModel(modelo);

			}else{
				JOptionPane.showMessageDialog(null,"Pe�a nao encontrada!");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
