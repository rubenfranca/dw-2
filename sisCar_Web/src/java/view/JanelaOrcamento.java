package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.*;
import dao.OrcamentoDAO;
import model.OrcamentoJanelaTableModel;
import model.OrcamentoTableModel;
import classes.Orcamentos;
import dao.PecaDAO;
import model.PecaTableModel;
import classes.Pecas;
import model.PecasComboBoxModel;
import classes.PecasOrcadas;
import dao.PecasOrcadasDAO;
import model.ServicoComboBoxModel;
import dao.ServicoDAO;
import model.ServicoTableModel;
import classes.Servicos;
import classes.ServicosOrcados;
import dao.ServicosOrcadosDAO;

public class JanelaOrcamento extends JFrame {

	private JanelaPecas teste = new JanelaPecas();;
	private JanelaServicos teste2 = null;
	private OrcamentoDAO orcamentoBanco;
	private Orcamentos novo;
	private PecasOrcadasDAO pecasOrcadasBanco;
	private ServicosOrcadosDAO ServicosOrcadosBanco;
	private JTable tabelaOrcamento;
	private PecaDAO pecaBanco;
	private int idOrca;
	private JTable tabelaPeca;
	private int idOrcamento;
	private JTextField tPecas;
	private JComboBox cServicos;
	private JComboBox cPecas;
	private ServicoDAO servicoBanco;

	JanelaOrcamento(){
		super("Tela de Or�amento"+"Siscar v1.0");
		setSize(800,800);
		JPanel painel = new JPanel(new BorderLayout());		
		setLocationRelativeTo(null);
		JPanel painelSuperior = new JPanel(new GridLayout(2,2,0,0));
		novoOrcamento();


		//botoes
		cServicos = new JComboBox();
		JButton bConsultaServico = new JButton("Consulta Servi�os");
		painelSuperior.add(cServicos);
		painelSuperior.add(bConsultaServico);
		cPecas = new JComboBox();
		JButton bConsultaPeca = new JButton("Consulta Pecas");
		painelSuperior.add(cPecas);
		painelSuperior.add(bConsultaPeca);
		preencheComboBoxServico();
		preencheComboBoxPecas();

		//tabela
		tabelaOrcamento = new JTable();
		JScrollPane barraRolagem = new JScrollPane(tabelaOrcamento);
		JPanel painelCentral = new JPanel(new BorderLayout());
		OrcamentoJanelaTableModel modelo = new OrcamentoJanelaTableModel();
		tabelaOrcamento.setModel(modelo);
		painelCentral.add("North",painelSuperior);
		painelCentral.add("Center",barraRolagem);	
		buscarDados();

		//painel.add("North",bar);
		painel.add("Center",painelCentral);

		bConsultaPeca.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				incluirPecas();

			}
		});
		bConsultaServico.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				incluirServicos();

			}
		});

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setContentPane(painel);
	}
	public void buscarDados() {

	}
	public void novoOrcamento(){
		novo = new Orcamentos(0,1,1);
		orcamentoBanco = new OrcamentoDAO();
		try {
			orcamentoBanco.salvar(novo);
			novo.setIdOrcamento(orcamentoBanco.getLastID());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public void incluirPecas(){
		Pecas selecao = (Pecas) cPecas.getSelectedItem();
		if(selecao != null){
			try {

				Orcamentos incluido = new Orcamentos(selecao.getIdPecas(),selecao.getNome(),selecao.getDescricao(),selecao.getValor(),1);
				OrcamentoJanelaTableModel modelo =  (OrcamentoJanelaTableModel) tabelaOrcamento.getModel();
				/*if(modelo.tamanho()>1){
		for(int i=0;i<modelo.tamanho();i++){
			if(modelo.getOrcamentos().get(i).getDescricao()==incluido.getDescricao()){
				modelo.getOrcamentos().get(i).setQuant(modelo.getOrcamentos().get(i).getQuant()+1);
				Orcamentos alterado = new Orcamentos(modelo.getOrcamentos().get(i).getIdItem(),modelo.getOrcamentos().get(i).getNome(),modelo.getOrcamentos().get(i).getDescricao(),modelo.getOrcamentos().get(i).getValor(),modelo.getOrcamentos().get(i).getQuant());
				modelo.removeRow(i);
				modelo.addRow(alterado);
			}else{
				modelo.addRow(incluido);
				break;
			}

		}
		}else{
			modelo.addRow(incluido);
		}*/
				modelo.addRow(incluido);
				PecasOrcadas nova = new PecasOrcadas(0,selecao.getIdPecas(),novo.getIdOrcamento());
				pecasOrcadasBanco = new PecasOrcadasDAO();


				pecasOrcadasBanco.salvar(nova,selecao);
				JOptionPane.showMessageDialog(null, "Peca adicionada ao orcamento!");
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	public void incluirServicos(){
		Servicos selecao = (Servicos) cServicos.getSelectedItem();
		if(selecao != null){
			try {
				Orcamentos incluido = new Orcamentos(selecao.getIdServicos(),selecao.getTipoServico(),selecao.getDescricao(),selecao.getValor(),1);
				OrcamentoJanelaTableModel modelo =  (OrcamentoJanelaTableModel) tabelaOrcamento.getModel();
				modelo.addRow(incluido);
				ServicosOrcados nova = new ServicosOrcados(0,selecao.getIdServicos(),novo.getIdOrcamento());
				ServicosOrcadosBanco = new ServicosOrcadosDAO();			
				ServicosOrcadosBanco.salvar(nova);
				JOptionPane.showMessageDialog(null, "Servico adicionado no orcamento!");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			JOptionPane.showMessageDialog(null, "selecione um servi�o!");
		}
	}
	public void preencheComboBoxServico() {
		try {
			List<Servicos> lista;
			servicoBanco = new ServicoDAO();
			lista = servicoBanco.listaTodos();
			ServicoComboBoxModel modelo = new ServicoComboBoxModel();
			for(Servicos s: lista) {
				modelo.addItem(s);
			}
			cServicos.setModel(modelo);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "problema ao carregar dados2!"+e.getMessage());
		}
	}
	public void preencheComboBoxPecas(){
		try {
			List<Pecas> lista;
			pecaBanco = new PecaDAO();
			lista = pecaBanco.listaTodos();
			PecasComboBoxModel modelo = new PecasComboBoxModel();
			for(Pecas p: lista) {
				modelo.addItem(p);
			}
			cPecas.setModel(modelo);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "problema ao carregar dados2!"+e.getMessage());
		}
	}

}