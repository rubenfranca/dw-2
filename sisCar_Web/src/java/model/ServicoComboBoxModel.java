package model;

import classes.Servicos;
import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class ServicoComboBoxModel extends AbstractListModel implements ComboBoxModel {


		private ArrayList<Servicos> dados;
		private Servicos selection;
		
		
		public ServicoComboBoxModel() {
			
			this.dados = new ArrayList<Servicos>();
		}
		
		public void addItem(Servicos c) {
			this.dados.add(c);
		}
		
		@Override
		public int getSize() {
			return this.dados.size();				
		}
		
	    public ArrayList<Servicos> getCurso() {
	    	
			return this.dados;
		}

		@Override
		public Object getElementAt(int index) {
			return this.dados.get(index);
		}
		

		@Override
		public Object getSelectedItem() {
			return selection;
		}

		@Override
		public void setSelectedItem(Object anItem) {
			selection = (Servicos) anItem;		
		}

	}
