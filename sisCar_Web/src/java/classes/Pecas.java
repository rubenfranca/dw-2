package classes;

public class Pecas {

	private int idPecas;
	private String nome;
	private String descricao;
	private float valor;
	private int quant;	
	
	public Pecas(int idPecas,String nome,String descricao,float valor,int quant){
		setIdPecas(idPecas);
		setDescricao(descricao);
		setValor(valor);
		setQuant(quant);
		setNome(nome);
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public float getValor() {
		return valor;
	}
	public void setValor(float valor) {
		this.valor = valor;
	}
	public int getQuant() {
		return quant;
	}
	public void setQuant(int quant) {
		this.quant = quant;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdPecas() {
		return idPecas;
	}
	public void setIdPecas(int idPecas) {
		this.idPecas = idPecas;
	}
	public String toString(){
		return getNome();
	}
}
