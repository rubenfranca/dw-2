package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import classes.Servicos;

import util.ConnectionDAO;
import util.DAO;

public class ServicoDAO implements DAO{

	private int lastID;
	private Connection conn;
	@Override
	public void atualizar(Object ob) throws Exception {
		Servicos servico = (Servicos) ob;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = ConnectionDAO.getConnection();
			String SQL = "UPDATE siscar.servicos SET nome = ?, tiposervico_idtiposervico=?, temposervico=?, descricao=?, valor=?"
					+ "WHERE siscar.servicos.idservicos = ?;";
			ps = conn.prepareStatement(SQL, java.sql.Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, servico.getNome());
			ps.setInt(2, Integer.parseInt(servico.getTipoServico()));
			ps.setInt(3,servico.getTempoServico());
			ps.setString(4, servico.getDescricao());
			ps.setFloat(5, servico.getValor());
			ps.setInt(6, servico.getIdServicos());
			int affectedRows = ps.executeUpdate();

			if (affectedRows == 0) 
				throw new SQLException();

			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				this.lastID = rs.getInt(1);
			}else{
				this.lastID = 0;
			}

		}catch (SQLException sqle){
			throw new Exception("Erro ao salvar dados do servico: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}

	}

	@Override
	public void excluir(Object ob) throws Exception {
		Servicos servico = (Servicos ) ob;

		if(servico != null) {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
				conn = ConnectionDAO.getConnection();
				String SQL = "DELETE FROM siscar.servicos WHERE siscar.servicos.idservicos = ?;";
				ps = conn.prepareStatement(SQL);
				ps.setInt(1, servico.getIdServicos());


				int affectedRows = ps.executeUpdate();

				if (affectedRows == 0) 
					throw new Exception("N�o foi possivel excluir!");

			}catch (SQLException sqle){
				throw new Exception("Erro ao remover servico: \n"+sqle);
			}finally{
				ConnectionDAO.closeConnection(conn,ps);
			}

		}else {
			throw new Exception("Objeto Passado é nulo!");
		}

	}

	@Override
	public List listaTodos() throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Servicos> servico = new ArrayList<>();

		try{
			String SQL = "SELECT * FROM siscar.view_servico";
			conn = ConnectionDAO.getConnection();
			ps = conn.prepareStatement(SQL);
			rs = ps.executeQuery();
			while(rs.next()) {
				servico.add(new Servicos(rs.getInt(1),rs.getString(2),rs.getString(6),rs.getInt(3),rs.getString(2),rs.getFloat(5)));
			}
		}catch (SQLException sqle){
			throw new Exception("Erro ao Carregar dados: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
		return servico;
	}

	@Override
	public List procura(Object ob) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Servicos> servico = new ArrayList<>();
		try{
			String SQL = "SELECT * FROM siscar.servicos INNER JOIN siscar.tiposervico "
					+ "ON siscar.servicos.tiposervico_idtiposervico = siscar.tiposervico.idtiposervico "
					+ "WHERE siscar.servicos.descricao LIKE ? ";
			ps = ConnectionDAO.getConnection().prepareStatement(SQL);
			ps.setString(1, "%"+(String)ob+"%");
			rs = ps.executeQuery();
			while(rs.next()){
				servico.add(new Servicos(rs.getInt(1),rs.getString(7),rs.getInt(3),rs.getString(4),rs.getFloat(5)));
			}

		}catch (SQLException sqle){
			throw new Exception("Erro ao carregar servicos: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
		return servico;
	}

	@Override
	public void salvar(Object ob) throws Exception {
		Servicos servico = (Servicos) ob;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try{
			conn = ConnectionDAO.getConnection();
			String SQL = "INSERT INTO siscar.servicos(nome, tiposervico_idtiposervico, temposervico, descricao,valor) "
					+ "VALUES (?, ?, ?, ?, ?);";
			ps = conn.prepareStatement(SQL, java.sql.Statement.RETURN_GENERATED_KEYS);
			ps.setString(1,servico.getNome());
			ps.setInt(2, Integer.parseInt(servico.getTipoServico()));
			ps.setInt(3,servico.getTempoServico());
			ps.setString(4, servico.getDescricao());
			ps.setFloat(5, servico.getValor());

			int affectedRows = ps.executeUpdate();

			if (affectedRows == 0) 
				throw new SQLException();

			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				this.lastID = rs.getInt(1);
			}else{
				this.lastID = 0;
			}

		}catch (SQLException sqle){
			if(conn != null){
				try{
					conn.setAutoCommit(false);
					System.err.print("Falha transacao em Rollback!!!!");
					conn.rollback();
				}catch(SQLException sqlE){
					sqlE.printStackTrace();
				}
			}
			//throw new Exception("Erro ao salvar dados dos servicos: \n"+sqle);
		}finally{
			ConnectionDAO.closeConnection(conn,ps);
		}
	}
	public int getLastID() {
		return this.lastID;
	}

}
